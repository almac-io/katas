# Katas

## Java

### Codewars

- https://www.codewars.com/kata/52756e5ad454534f220001ef/java
- https://www.codewars.com/kata/52223df9e8f98c7aa7000062/train/java
- https://www.codewars.com/kata/5259510fc76e59579e0009d4/java
- https://www.codewars.com/kata/54acc128329e634e9a000362
- https://www.codewars.com/kata/5531abe4855bcc8d1f00004c/java

## Kotlin

### Codewars

- ?

## Haskell

### Codewars

- ?

## TypeScript / JavaScript

### Codewars

- ?
