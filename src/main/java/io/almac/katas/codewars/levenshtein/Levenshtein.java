package io.almac.katas.codewars.levenshtein;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Levenshtein {
  private final Object o = new Object();

  public String findMostCommon(String[] words, String to) {
    String mostCommon = "";
    int min = Integer.MAX_VALUE;
    final Map<String, Integer> distances = new HashMap<>();
    final List<Thread> threads = new LinkedList<>();
    for (String word : words) {
      Thread t = new Thread(() -> {
        int currentDistance = distance(word, to);
        synchronized (o) {
          distances.put(word, currentDistance);
        }
      });
      threads.add(t);
      t.start();
    }
    for (Thread thread : threads) {
      try {
        thread.join();
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }
    System.out.println(distances);
    for (Map.Entry<String, Integer> entry : distances.entrySet()) {
      if (entry.getValue() < min) {
        min = entry.getValue();
        mostCommon = entry.getKey();
      }
    }
    return mostCommon;
  }

  public int distance(String a, String b) {
    if (a.isBlank()) {
      return b.length();
    }
    if (b.isBlank()) {
      return a.length();
    }
    if (a.charAt(0) == b.charAt(0)) {
      return distance(a.substring(1), b.substring(1));
    }
    return 1 + Math.min(
       distance(a.substring(1), b),
       Math.min(
          distance(a, b.substring(1)),
          distance(a.substring(1), b.substring(1))
       )
    );
  }
}
