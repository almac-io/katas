package io.almac.katas.codewars.rot13;

public class Symbol {
  private static final int LOWERCASE_LOWER_BOUND = 65;
  private static final int LOWERCASE_UPPER_BOUND = 90;
  private static final int UPPERCASE_LOWER_BOUND = 97;
  private static final int UPPERCASE_UPPER_BOUND = 122;

  private final int intRepresentation;
  private final int rotationSteps;

  public Symbol(char character, int rotationSteps) {
    intRepresentation = character;
    this.rotationSteps = rotationSteps;
  }

  public int newIndex() {
    if (!isLetter()) {
      return intRepresentation;
    }
    int lowerBounds = isLowerCase() ? LOWERCASE_LOWER_BOUND : UPPERCASE_LOWER_BOUND;
    int upperBounds = isLowerCase() ? LOWERCASE_UPPER_BOUND : UPPERCASE_UPPER_BOUND;

    int newCharIndex = intRepresentation;
    if (intRepresentation >= lowerBounds && intRepresentation <= upperBounds) {
      newCharIndex = intRepresentation + rotationSteps;
      if (newCharIndex > upperBounds) {
        newCharIndex = lowerBounds + ((intRepresentation + rotationSteps) % upperBounds) - 1;
      }
    }

    return newCharIndex;
  }

  public boolean isLetter() {
    return isLowerCase() || isUpperCase();
  }

  private boolean isLowerCase() {
    return intRepresentation >= LOWERCASE_LOWER_BOUND && intRepresentation <= LOWERCASE_UPPER_BOUND;
  }

  private boolean isUpperCase() {
    return intRepresentation >= UPPERCASE_LOWER_BOUND && intRepresentation <= UPPERCASE_UPPER_BOUND;
  }
}
