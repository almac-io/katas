package io.almac.katas.codewars.rot13;

public class Rotation {
  private static final int DEFAULT = 13;
  private final int steps;

  public Rotation() {
    this(DEFAULT);
  }

  public Rotation(int steps) {
    this.steps = steps;
  }

  public String apply(String input) {
    final StringBuilder sb = new StringBuilder();
    for (int i = 0; i < input.length(); i++) {
      char character = input.charAt(i);
      int newCharIndex = new Symbol(character, steps).newIndex();
      sb.append((char) newCharIndex);
    }
    return sb.toString();
  }

  public int getSteps() {
    return steps;
  }

  public Rotation by(int amountOfSteps) {
    return new Rotation(amountOfSteps);
  }
}
