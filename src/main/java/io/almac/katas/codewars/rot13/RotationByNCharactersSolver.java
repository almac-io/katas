package io.almac.katas.codewars.rot13;

// For https://www.codewars.com/kata/52223df9e8f98c7aa7000062/java
public class RotationByNCharactersSolver {

  private final Rotation r;

  public RotationByNCharactersSolver() {
    r = new Rotation();
  }

  public RotationByNCharactersSolver(int n) {
    r = new Rotation(n);
  }

  public String solve(String input) {
    if (r.getSteps() == 13) {
      return r.apply(input);
    }
    return r.by(Math.max(0, 13 - r.getSteps() % 13)).apply(input)
       + "\n"
       + r.by(Math.min(26, 13 + r.getSteps() % 13)).apply(input);
  }
}
