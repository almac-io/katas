package io.almac.katas.codewars.rot13;

public class Solution {

  public static String rot13(String message) {
    return new RotationByNCharactersSolver().solve(message);
  }

  static class RotationByNCharactersSolver {

    private final Rotation r;

    public RotationByNCharactersSolver() {
      r = new Rotation();
    }

    public RotationByNCharactersSolver(int n) {
      r = new Rotation(n);
    }

    public String solve(String input) {
      if (r.getSteps() == 13) {
        return r.apply(input);
      }
      return r.by(Math.max(0, 13 - r.getSteps() % 13)).apply(input)
         + "\n"
         + r.by(Math.min(26, 13 + r.getSteps() % 13)).apply(input);
    }
  }

  static class Symbol {
    private static final int LOWERCASE_LOWER_BOUND = 65;
    private static final int LOWERCASE_UPPER_BOUND = 90;
    private static final int UPPERCASE_LOWER_BOUND = 97;
    private static final int UPPERCASE_UPPER_BOUND = 122;

    private final int intRepresentation;
    private final int rotationSteps;

    public Symbol(char character, int rotationSteps) {
      intRepresentation = character;
      this.rotationSteps = rotationSteps;
    }

    public int newIndex() {
      if (!isLetter()) {
        return intRepresentation;
      }
      int lowerBounds = isLowerCase() ? LOWERCASE_LOWER_BOUND : UPPERCASE_LOWER_BOUND;
      int upperBounds = isLowerCase() ? LOWERCASE_UPPER_BOUND : UPPERCASE_UPPER_BOUND;

      int newCharIndex = intRepresentation;
      if (intRepresentation >= lowerBounds && intRepresentation <= upperBounds) {
        newCharIndex = intRepresentation + rotationSteps;
        if (newCharIndex > upperBounds) {
          newCharIndex = lowerBounds + ((intRepresentation + rotationSteps) % upperBounds) - 1;
        }
      }

      return newCharIndex;
    }

    public boolean isLetter() {
      return isLowerCase() || isUpperCase();
    }

    private boolean isLowerCase() {
      return intRepresentation >= LOWERCASE_LOWER_BOUND && intRepresentation <= LOWERCASE_UPPER_BOUND;
    }

    private boolean isUpperCase() {
      return intRepresentation >= UPPERCASE_LOWER_BOUND && intRepresentation <= UPPERCASE_UPPER_BOUND;
    }
  }

  static class Rotation {
    private static final int DEFAULT = 13;
    private final int steps;

    public Rotation() {
      this(DEFAULT);
    }

    public Rotation(int steps) {
      this.steps = steps;
    }

    public String apply(String input) {
      final StringBuilder sb = new StringBuilder();
      for (int i = 0; i < input.length(); i++) {
        char character = input.charAt(i);
        int newCharIndex = new Symbol(character, steps).newIndex();
        sb.append((char) newCharIndex);
      }
      return sb.toString();
    }

    public int getSteps() {
      return steps;
    }

    public Rotation by(int amountOfSteps) {
      return new Rotation(amountOfSteps);
    }
  }
}
