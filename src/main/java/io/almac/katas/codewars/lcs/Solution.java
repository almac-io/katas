package io.almac.katas.codewars.lcs;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

public class Solution {

  public static String lcs(String x, String y) {
    return new LcsSolver().solve(x, y);
  }

  static class SolutionFinder {

    public String findSolution(Set<String> firstSolution, Set<String> secondSolution) {
      final var solution = new HashSet<>(firstSolution);
      solution.retainAll(secondSolution);
      return solution.stream()
         .max(Comparator.comparingInt(String::length))
         .orElse("");
    }
  }

  static class RecursiveSequencePartitioner {
    public Set<String> partition(String sequence) {
      Set<String> res = new HashSet<>();
      for (int i = 1; i <= sequence.length(); i++) {
        pick(i, "", sequence, res);
      }
      return res;
    }

    private void pick(int length, String currentElement, String sequence, Set<String> res) {
      if (currentElement.length() == length) {
        res.add(currentElement);
        currentElement = "";
      }

      for (int i = 0; i < sequence.length(); i++) {
        pick(length, currentElement + sequence.charAt(i), sequence.substring(i + 1), res);
      }
    }
  }

  static class LcsSolver {

    private final RecursiveSequencePartitioner partitioner;
    private final SolutionFinder finder;

    public LcsSolver() {
      this(
         new RecursiveSequencePartitioner(),
         new SolutionFinder()
      );
    }

    public LcsSolver(RecursiveSequencePartitioner partitioner, SolutionFinder finder) {
      this.partitioner = partitioner;
      this.finder = finder;
    }

    public String solve(String sequence1, String sequence2) {
      final var combinationsSequence1 = this.partitioner.partition(sequence1);
      final var combinationsSequence2 = this.partitioner.partition(sequence2);
      return finder.findSolution(combinationsSequence1, combinationsSequence2);
    }
  }
}
