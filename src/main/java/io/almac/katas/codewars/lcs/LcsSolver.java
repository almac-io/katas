package io.almac.katas.codewars.lcs;

public class LcsSolver {

  private final RecursiveSequencePartitioner partitioner;
  private final SolutionFinder finder;

  public LcsSolver() {
    this(
       new RecursiveSequencePartitioner(),
       new SolutionFinder()
    );
  }

  public LcsSolver(RecursiveSequencePartitioner partitioner, SolutionFinder finder) {
    this.partitioner = partitioner;
    this.finder = finder;
  }

  public String solve(String sequence1, String sequence2) {
    final var combinationsSequence1 = this.partitioner.partition(sequence1);
    final var combinationsSequence2 = this.partitioner.partition(sequence2);
    return finder.findSolution(combinationsSequence1, combinationsSequence2);
  }
}
