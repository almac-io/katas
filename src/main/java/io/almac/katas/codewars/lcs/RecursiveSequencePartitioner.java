package io.almac.katas.codewars.lcs;

import java.util.HashSet;
import java.util.Set;

public class RecursiveSequencePartitioner {
  public Set<String> partition(String sequence) {
    Set<String> res = new HashSet<>();
    for (int i = 1; i <= sequence.length(); i++) {
      pick(i, "", sequence, res);
    }
    return res;
  }

  private void pick(int length, String currentElement, String sequence, Set<String> res) {
    if (currentElement.length() == length) {
      res.add(currentElement);
      currentElement = "";
    }

    for (int i = 0; i < sequence.length(); i++) {
      pick(length, currentElement + sequence.charAt(i), sequence.substring(i + 1), res);
    }
  }
}
