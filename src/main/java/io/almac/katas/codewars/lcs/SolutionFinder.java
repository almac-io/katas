package io.almac.katas.codewars.lcs;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

public class SolutionFinder {

  public String findSolution(Set<String> firstSolution, Set<String> secondSolution) {
    final var solution = new HashSet<>(firstSolution);
    solution.retainAll(secondSolution);
    return solution.stream()
       .max(Comparator.comparingInt(String::length))
       .orElse("");
  }
}
