package io.almac.katas.codewars.statemachine;

import java.util.HashMap;
import java.util.Map;

public class Statemachine {

  enum Events {
    APP_PASSIVE_OPEN,
    APP_ACTIVE_OPEN,
    APP_SEND,
    APP_CLOSE,
    APP_TIMEOUT,
    RCV_SYN,
    RCV_ACK,
    RCV_SYN_ACK,
    RCV_FIN,
    RCV_FIN_ACK
  }

  enum States {
    CLOSED,
    LISTEN,
    SYN_SENT,
    SYN_RCVD,
    ESTABLISHED,
    CLOSE_WAIT,
    LAST_ACK,
    FIN_WAIT_1,
    FIN_WAIT_2,
    CLOSING,
    ERROR, TIME_WAIT
  }

  private Map<States, Map<Events, States>> rules = new HashMap<>();
  private final static String RULES = """
    CLOSED: APP_PASSIVE_OPEN -> LISTEN
    CLOSED: APP_ACTIVE_OPEN  -> SYN_SENT
    LISTEN: RCV_SYN          -> SYN_RCVD
    LISTEN: APP_SEND         -> SYN_SENT
    LISTEN: APP_CLOSE        -> CLOSED
    SYN_RCVD: APP_CLOSE      -> FIN_WAIT_1
    SYN_RCVD: RCV_ACK        -> ESTABLISHED
    SYN_SENT: RCV_SYN        -> SYN_RCVD
    SYN_SENT: RCV_SYN_ACK    -> ESTABLISHED
    SYN_SENT: APP_CLOSE      -> CLOSED
    ESTABLISHED: APP_CLOSE   -> FIN_WAIT_1
    ESTABLISHED: RCV_FIN     -> CLOSE_WAIT
    FIN_WAIT_1: RCV_FIN      -> CLOSING
    FIN_WAIT_1: RCV_FIN_ACK  -> TIME_WAIT
    FIN_WAIT_1: RCV_ACK      -> FIN_WAIT_2
    CLOSING: RCV_ACK         -> TIME_WAIT
    FIN_WAIT_2: RCV_FIN      -> TIME_WAIT
    TIME_WAIT: APP_TIMEOUT   -> CLOSED
    CLOSE_WAIT: APP_CLOSE    -> LAST_ACK
    LAST_ACK: RCV_ACK        -> CLOSED
    """;

  public Statemachine() {
    this(RULES);
  }

  public Statemachine(final String rules) {
    for (String rule : rules.split("\n")) {
      processRule(rule);
    }
  }

  public States determineNextState(String[] events) {
    var result = States.CLOSED;
    for (String event : events) {
      result = rules.get(result).get(Events.valueOf(event));
      if (result == null) {
        return States.ERROR;
      }
    }
    return result;
  }

  public void processRule(String rule) {
    final String cleanedUp = rule.replaceAll(" ", "");
    String state = cleanedUp.substring(0, cleanedUp.indexOf(":"));
    String event = cleanedUp.substring(cleanedUp.indexOf(":") + 1, cleanedUp.indexOf("-"));
    String targetState = cleanedUp.substring(cleanedUp.indexOf(">") + 1);
    rules.putIfAbsent(States.valueOf(state), new HashMap<>());
    final var inner = rules.get(States.valueOf(state));
    inner.putIfAbsent(Events.valueOf(event), States.valueOf(targetState));
  }
}
