package io.almac.katas.codewars.statemachine;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class StatemachineTest {

  private Statemachine statemachine;

  public static Stream<Arguments> statemachineProvider() {
    return Stream.of(Arguments.of(
        new String[]{"APP_PASSIVE_OPEN", "APP_SEND", "RCV_SYN_ACK"},
        Statemachine.States.ESTABLISHED
      ),
      Arguments.of(
        new String[]{"APP_ACTIVE_OPEN"},
        Statemachine.States.SYN_SENT
      ),
      Arguments.of(
        new String[]{"APP_ACTIVE_OPEN", "RCV_SYN_ACK", "APP_CLOSE", "RCV_FIN_ACK", "RCV_ACK"},
        Statemachine.States.ERROR
      ));
  }

  @BeforeEach
  void setup() {
    statemachine = new Statemachine();
  }

  @ParameterizedTest
  @MethodSource("statemachineProvider")
  void determineNextState(String[] events, Statemachine.States expectedState) {
    final var state = statemachine.determineNextState(events);
    assertThat(state).isEqualTo(expectedState);
  }
}