package io.almac.katas.codewars.rot13;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class RotationByNCharactersSolverTest {

  private RotationByNCharactersSolver solver = new RotationByNCharactersSolver();

  public static Stream<Arguments> solveInputs() {
    return Stream.of(
       Arguments.of(
          "EBG13 rknzcyr.", "ROT13 example."
       ),
       Arguments.of(
          "This is my first ROT13 excercise!", "Guvf vf zl svefg EBG13 rkprepvfr!"
       )
    );
  }

  @BeforeEach
  void setup() {
    solver = new RotationByNCharactersSolver();
  }

  @ParameterizedTest
  @MethodSource("solveInputs")
  void solve(String input, String expected) {
    final var actual = solver.solve(input);
    assertThat(actual).isEqualTo(expected);
  }

}