package io.almac.katas.codewars.rot13;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class RotationTest {

  private Rotation rotation = new Rotation();

  @BeforeEach
  void setup() {
    this.rotation = new Rotation();
  }

  @Test
  void testRotation() {
    final var result = rotation.apply("EBG13 rknzcyr.");
    assertThat(result).isEqualTo("ROT13 example.");
  }

}