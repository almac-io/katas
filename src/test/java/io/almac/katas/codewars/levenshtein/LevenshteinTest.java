package io.almac.katas.codewars.levenshtein;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class LevenshteinTest {

  private Levenshtein levenshtein;

  @BeforeEach
  void setup() {
    this.levenshtein = new Levenshtein();
  }

  @Test
  void testFindMostCommon() {
    final var res = levenshtein.findMostCommon(
       new String[]{"cherry", "pineapple", "melon", "strawberry", "raspberry"},
       "berry"
    );

    assertThat(res).isEqualTo("cherry");
  }

  @Test
  void test() {
    final var res = this.levenshtein.findMostCommon(
      new String[]{
        "psaysnhfrrqgxwik",
        "pdyjrkaylryr",
        "lnjhrzfrosinb",
        "afirbipbmkamjzw",
        "loogviwcojxgvi",
        "iqkyztorburjgiudi",
        "cwhyyzaorpvtnlfr",
        "iroezmixmberfr",
        "jhjyasikwyufr",
        "tklquxrnhfiggb",
        "cpnqknjyviusknmte",
        "hwzsemiqxjwfk",
        "ntwmwwmicnjvhtt",
        "emvquxrvvlvwvsi",
        "sefsknopiffajor",
        "znystgvifufptxr",
        "xuwahveztwoor",
        "hrwuhmtxxvmygb",
        "karpscdigdvucfr",
        "xrgdgqfrldwk",
        "nnsoamjkrzgldi",
        "ljxzjjorwgb",
        "cfvruditwcxr",
        "eglanhfredaykxr",
        "fxjskybblljqr",
        "qifwqgdsijibor",
        "xikoctmrhpvi",
        "ajacizfrgxfumzpvi",
        "mhmkakybpczjbb",
        "vkholxrvjwisrk",
        "npyrgrpbdfqhhncdi",
        "pxyousorusjxxbt",
        "jcocndjkyb",
        "fxpvfhfrujjaifr",
        "hkldhadcxrjbmkmcdi",
        "hirldidcuzbyb",
        "ggcvrtxrtnafw",
        "tdvibqccxr",
        "osbednerciaai",
        "qojfrlhufr",
        "kqijoorfkejdcxr",
        "zqdrhpviqslik",
        "clxmqmiycvidiyr",
        "xffrkbdyjveb",
        "dyhxgviphoptak",
        "dihhiczkdwiofpr",
        "riyhpvimgaliuxr",
        "fgtrjakzlnaebxr",
        "ppctybxgtleipb",
        "ucxmdeudiycokfnb"
      },
      "rkacypviuburk"
    );
    System.out.println(res);
  }

  @ParameterizedTest
  @MethodSource("levenshteinExamples")
  void testLevenshtein(String a, String b, int expectedDistance) {
    assertThat(levenshtein.distance(a, b)).isEqualTo(expectedDistance);
  }

  public static Stream<Arguments> levenshteinExamples() {
    return Stream.of(
       Arguments.of("kitten", "sitten", 1),
       Arguments.of("sitten", "sittin", 1),
       Arguments.of("sittin", "sitting", 1),
       Arguments.of("uninformed", "uniformed", 1),
       Arguments.of("beer", "wine", 4),
       Arguments.of("cherry", "berry", 2),
       Arguments.of("raspberry", "berry", 4)
    );
  }

}