package io.almac.katas.codewars.levenshtein;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class LevenshteinWithCapTest {

  private LevenshteinWithCap levenshteinWithCap;

  @BeforeEach
  void setup() {
  }

  @Test
  void test() {
    this.levenshteinWithCap = new LevenshteinWithCap("rkacypviuburk".length());
    final var res = this.levenshteinWithCap.findMostCommon(
      new String[]{
        "psaysnhfrrqgxwik",
        "pdyjrkaylryr",
        "lnjhrzfrosinb",
        "afirbipbmkamjzw",
        "loogviwcojxgvi",
        "iqkyztorburjgiudi",
        "cwhyyzaorpvtnlfr",
        "iroezmixmberfr",
        "jhjyasikwyufr",
        "tklquxrnhfiggb",
        "cpnqknjyviusknmte",
        "hwzsemiqxjwfk",
        "ntwmwwmicnjvhtt",
        "emvquxrvvlvwvsi",
        "sefsknopiffajor",
        "znystgvifufptxr",
        "xuwahveztwoor",
        "hrwuhmtxxvmygb",
        "karpscdigdvucfr",
        "xrgdgqfrldwk",
        "nnsoamjkrzgldi",
        "ljxzjjorwgb",
        "cfvruditwcxr",
        "eglanhfredaykxr",
        "fxjskybblljqr",
        "qifwqgdsijibor",
        "xikoctmrhpvi",
        "ajacizfrgxfumzpvi",
        "mhmkakybpczjbb",
        "vkholxrvjwisrk",
        "npyrgrpbdfqhhncdi",
        "pxyousorusjxxbt",
        "jcocndjkyb",
        "fxpvfhfrujjaifr",
        "hkldhadcxrjbmkmcdi",
        "hirldidcuzbyb",
        "ggcvrtxrtnafw",
        "tdvibqccxr",
        "osbednerciaai",
        "qojfrlhufr",
        "kqijoorfkejdcxr",
        "zqdrhpviqslik",
        "clxmqmiycvidiyr",
        "xffrkbdyjveb",
        "dyhxgviphoptak",
        "dihhiczkdwiofpr",
        "riyhpvimgaliuxr",
        "fgtrjakzlnaebxr",
        "ppctybxgtleipb",
        "ucxmdeudiycokfnb"
      },
      "rkacypviuburk"
    );
    assertThat(res).isEqualTo("zqdrhpviqslik");
  }

}