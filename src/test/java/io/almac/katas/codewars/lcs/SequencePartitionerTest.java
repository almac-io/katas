package io.almac.katas.codewars.lcs;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Set;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class SequencePartitionerTest {

  private RecursiveSequencePartitioner sequencePartitioner;

  @BeforeEach
  void setup() {
    sequencePartitioner = new RecursiveSequencePartitioner();
  }

  @ParameterizedTest
  @MethodSource("partitionProvider")
  void testCreatePartitionsRecursively(String sequence, Set<String> expectedPartitions) {
    final var recursiveSequencePartitioner = new RecursiveSequencePartitioner();
    final var partitions = recursiveSequencePartitioner.partition(sequence);
    System.out.println(partitions);
    assertThat(partitions)
       .hasSameSizeAs(expectedPartitions)
       .containsAll(expectedPartitions);
  }

  public static Stream<Arguments> partitionProvider() {
    return Stream.of(
       Arguments.of("a", Set.of("a")),
       Arguments.of("ab", Set.of("a", "b", "ab")),
       Arguments.of(
          "abc",
          Set.of("a", "b", "c", "ab", "ac", "bc", "abc")
       ),
       Arguments.of(
          "abcd",
          Set.of("a", "b", "c", "d", "ab", "ac", "ad", "bc", "bd", "cd", "abc", "bcd", "acd", "abd", "abcd")
       ),
       Arguments.of(
          "abcde",
          Set.of("de",
             "bc",
             "bd",
             "ace",
             "bcd",
             "be",
             "acd",
             "bce",
             "acde",
             "abde",
             "a",
             "ab",
             "cd",
             "b",
             "ce",
             "ac",
             "abd",
             "bde",
             "c",
             "ad",
             "abc",
             "ade",
             "d",
             "ae",
             "e",
             "abe",
             "cde",
             "abcde",
             "bcde",
             "abce",
             "abcd")
       )
    );
  }
}