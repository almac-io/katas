package io.almac.katas.codewars.lcs;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Set;
import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class SolutionFinderTest {

    private SolutionFinder solutionFinder = new SolutionFinder();

    @BeforeEach
    void setup() {
        solutionFinder = new SolutionFinder();
    }

    @ParameterizedTest
    @MethodSource("solutionProvider")
    void testFindSolution(Set<String> firstSolution, Set<String> secondSolution, String expectedResult) {
        final var result = solutionFinder.findSolution(firstSolution, secondSolution);
        assertThat(result).isEqualTo(expectedResult);
    }

    public static Stream<Arguments> solutionProvider() {
        return Stream.of(
                Arguments.of(
                        Set.of("a", "b", "c"),
                        Set.of("a", "d", "f"),
                        "a"
                )
        );
    }

}