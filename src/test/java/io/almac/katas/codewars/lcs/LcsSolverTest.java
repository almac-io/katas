package io.almac.katas.codewars.lcs;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class LcsSolverTest {

    private LcsSolver solver;

    @BeforeEach
    void setup() {
        solver = new LcsSolver();
    }

    @Test
    void testSolvingPath() {
        final var partitioner = mock(RecursiveSequencePartitioner.class);
        final var finder = mock(SolutionFinder.class);

        new LcsSolver(
                partitioner,
                finder
        ).solve("abc", "def");

        verify(partitioner, times(1)).partition("abc");
        verify(partitioner, times(1)).partition("def");
        verify(finder, times(1)).findSolution(anySet(), anySet());
    }

    @ParameterizedTest
    @MethodSource("inputProvider")
    void test(String sequence1, String sequence2, String expected) {
        final var res = solver.solve(sequence1, sequence2);
        assertThat(expected).isEqualTo(res);
    }

    static Stream<Arguments> inputProvider() {
       return Stream.of(
               Arguments.of("abcdef", "abc", "abc"),
               Arguments.of("abcdef", "acf", "acf"),
               Arguments.of("132535365", "123456789", "12356")
       );
    }

}